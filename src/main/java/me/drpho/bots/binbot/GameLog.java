package me.drpho.bots.binbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Comma separated strings that represent game events
 * Strings are in the format:
 * Epoch timestamp, event type code, data
 * Event types | type code | (primary data) | data format
 * Game start  | 0 - 0b00000 | (str) - spotify playlist to use
 * Music start | 1 - 0b00001 | (int)-int - number of songs loaded '-' number of players at start
 * Game over   | 2 - 0b00010 | (long)-int-int - winner snowflake '-' winner points '-' reason metadata
 *
 * Game end format - int 0b0ss...ssrr
 * | 0 -> last round
 * | 1 -> VC empty
 * | 2 -> no players still playing
 *
 * Player join initial | 8 - 0b01000 | (long) - player snowflake
 * Player leave VC     | 9 - 0b01001 | ```
 * Player rejoin VC    | 10- 0b01010 | ```
 * Player remessage bot| 11- 0b01011 | ```
 * Player asks to stop | 12- 0b01100 | ```
 *
 * Non guess msg | 16- 0b10000 | (long)-str - player snowflake '-' base64 encoded content (not in round)
 * Player guess  | 17- 0b10001 | (long)-int-float(##.###)-str - player snowflake '-' guess metadata '-' guess time '-' base64 encoded content
 * Song start    | 18- 0b10010 | (str)-int-str - spotify id '-' round # '-' base64 encoded formatted song string
 * Song ended    | 19- 0b10011 | [(long-int)] - array of (player snowflake '-' score metadata), ordered by total points
 *
 * Guess format - int 0b000abcdooooooppppzzzzzzz.zzzzzzzzzz
 * a - correct song?
 * b - correct artist?
 * c - has now guessed song?
 * d - has now guessed artist?
 * o - preliminary position (int)
 * p - preliminary points for the round (int)
 *
 * Score format - int 0b0000saggggggppppoooooottttttttt
 * s - correct song?
 * a - correct artist?
 * g - guess position (int)
 * p - points for the round (int)
 * o - leaderboard order (int)
 * t - total points (int)
 */
public class GameLog {
	private final Map<Long, String> gameLog;
	private final List<Event> eventBuffer;
	private final String guildId;
	private String gameId = "";

	private static final Base64.Encoder encoder = Base64.getEncoder();

	private static final Logger logger = LoggerFactory.getLogger(GameLog.class);

	public GameLog(String guildId) {
		this.guildId = guildId;
		gameLog = new ConcurrentSkipListMap<>();
		eventBuffer = new Vector<>();
	}

	public List<String> getEventLog() {
		return new ArrayList<>(gameLog.values());
	}

	public void flushEventBuffer() {
		try {
			DataManager.addEvents(guildId, gameId, eventBuffer);
			eventBuffer.clear();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}

	public String getEventLogAsString() {
		List<String> list = getEventLog();
		StringBuilder ret = new StringBuilder();
		list.forEach(item -> ret.append(item).append("\n"));
		return ret.toString();
	}

	private void createLineStr(String type, String primaryData, String secondaryData, String secondaryDataCondensed) {
		long currTime = System.currentTimeMillis();
		String logItem = String.format("%s,%s,%s,%s", currTime, type, primaryData, secondaryData);
		gameLog.put(currTime, logItem);
		eventBuffer.add(new Event(type, currTime, primaryData, secondaryData));
		logger.info("New log item for game {}: {}", gameId, logItem);
	}

	public void gameStart(String playlistId, String gameId) {
		this.gameId = gameId;
		createLineStr("GMSTRT", playlistId, "", null);
	}

	public void musicStart(int songsLoaded, int players) {
		createLineStr("MSSTRT", Integer.toString(songsLoaded), Integer.toString(players), null);
	}

	public void gameOver(long winner, int points, int reason) {
		assert reason < 4 && reason >= 0;

		int data = points << 2 | reason;
		createLineStr("GMOVER", Long.toString(winner), Integer.toString(data), null);
	}

	public void playerJoin(long player) {
		createLineStr("PLJOIN", Long.toString(player), "", null);
	}

	public void playerLeaveVC(long player) {
		createLineStr("PLLVVC", Long.toString(player), "", null);
	}

	public void playerRejoinVC(long player) {
		createLineStr("PLRJVC", Long.toString(player), "", null);
	}

	public void playerJoinMsg(long player) {
		createLineStr("PLJNMS", Long.toString(player), "", null);
	}

	public void playerLeaveMsg(long player) {
		createLineStr("PLLVMS", Long.toString(player), "", null);
	}

	public void nonGuessMsg(long player, String content) {
		createLineStr("OTRMSG", Long.toString(player), encoder.encodeToString(content.getBytes()), null);
	}

	public void guess(long player, GuessData data, double guessTime, String content) {
		if (guessTime == Double.POSITIVE_INFINITY)
			guessTime = 0;
		createLineStr("GUESS", Long.toString(player), String.format("%s-%.3f-%s", data.getFullMetadata(), guessTime, encoder.encodeToString(content.getBytes())),
				String.format("%d-%.3f-%s", data.getMetadataInt(), guessTime, encoder.encodeToString(content.getBytes())));
	}

	public void songStart(int round, String songId, String songInfo) {
		createLineStr("SNGSRT", songId, String.format("%d-%s", round, encoder.encodeToString(songInfo.getBytes())), null);
	}

	public void songEnd(Map<Long, RoundData> players) {
		StringBuilder data = new StringBuilder();
		StringBuilder condensedData = new StringBuilder();
		for (Map.Entry<Long, RoundData> entry : players.entrySet()) {
			data.append(String.format("(%s-%s)", entry.getKey(), entry.getValue().getFullMetadata()));
			condensedData.append(String.format("(%s-%d)", entry.getKey(), entry.getValue().getMetadataInt()));
		}
		createLineStr("SNGEND", "", data.toString(), condensedData.toString());
	}

	public static class GuessData {
		private final boolean guessSong, guessArtist, hasSong, hasArtist;
		private final int pos, points;

		public GuessData(boolean guessSong, boolean guessArtist, boolean hasSong, boolean hasArtist, int pos, int points) {
			this.guessSong = guessSong;
			this.guessArtist = guessArtist;
			this.hasSong = hasSong;
			this.hasArtist = hasArtist;
			this.pos = pos;
			this.points = points;
		}

		private int getMetadataInt() {
			assert pos < 64 && pos >= 0;
			assert points < 16 && points >= 0;

			int metadata = 0;
			metadata |= guessSong ? 1 << 13 : 0;
			metadata |= guessArtist ? 1 << 12 : 0;
			metadata |= hasSong ? 1 << 11 : 0;
			metadata |= hasArtist ? 1 << 10 : 0;
			metadata |= pos << 4;
			metadata |= points;
			return metadata;
		}

		private String getFullMetadata() {
			String ret = "";
			ret += guessSong ? "y-" : "n-";
			ret += guessArtist ? "y-" : "n-";
			ret += hasSong ? "y-" : "n-";
			ret += hasArtist ? "y-" : "n-";
			ret += pos + "-" + points;
			return ret;
		}
	}

	public static class RoundData {
		private final boolean song, artist;
		private final int guessPos, roundPoints, totalPos, totalPoints;

		public RoundData(boolean song, boolean artist, int guessPos, int roundPoints, int totalPos, int totalPoints) {
			this.song = song;
			this.artist = artist;
			this.guessPos = guessPos;
			this.roundPoints = roundPoints;
			this.totalPos = totalPos;
			this.totalPoints = totalPoints;
		}

		private int getMetadataInt() {
			assert guessPos < 64 && guessPos >= 0;
			assert roundPoints < 16 && roundPoints >= 0;
			assert totalPos < 64 && totalPos >= 0;
			assert totalPoints < 512 && totalPoints >= 0;

			int metadata = 0;
			metadata |= song ? 1 << 26 : 0;
			metadata |= artist ? 1 << 25 : 0;
			metadata |= guessPos << 19;
			metadata |= roundPoints << 15;
			metadata |= totalPos << 9;
			metadata |= totalPoints;
			return metadata;
		}

		private String getFullMetadata() {
			String ret = "";
			ret += song ? "y-" : "n-";
			ret += artist ? "y-" : "n-";
			ret += guessPos + "-";
			ret += roundPoints + "-";
			ret += totalPos + "-";
			ret += totalPoints;
			return ret;
		}
	}
}
