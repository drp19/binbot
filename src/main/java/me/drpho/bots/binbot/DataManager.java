package me.drpho.bots.binbot;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class DataManager {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	private static Connection conn;

	/**
	 * Connect to the database
	 * @param fileName the database file name
	 * @throws SQLException failure connecting or creating tables - fatal error
	 */
	public static void connect(@NonNull String fileName) throws SQLException {
		File f = new File(fileName);
		String url = "jdbc:sqlite:" + f.getAbsolutePath();
		try {
			conn = DriverManager.getConnection(url);
			logger.debug("Database driver: {}", conn.getMetaData().getDriverName());
			logger.info("Database connection established.");
			createTables();
		} catch (SQLException e) {
			logger.error("SQL Connect Error: {}", e.getMessage());
			throw e;
		}
	}

	/**
	 * Returns a list of song IDs from a playlist
	 * Note: will return null (not found) on any error
	 * @param id The playlist ID to retrieve
	 * @return Array of songs, or null if not found
	 */
	@Nullable
	public static String[] getPlaylist(@NonNull String id) {
		String sql = "SELECT songs "
				+ "FROM playlists WHERE playlist_id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getString("songs").split(",");
			}
			logger.info("No playlist {} found.", id);
		} catch (SQLException e) {
			logger.error("SQL Query error: {}", e.getMessage());
		}
		return null;
	}

	/**
	 * Returns the time that a playlist was updated in the database
	 * Note: returns empty on errors
	 * @param id The spotify id
	 * @return An optional timestamp
	 */
	@NonNull
	public static Optional<Long> getUpdatedTime(@NonNull String id)  {
		String sql = "SELECT last_updated "
				+ "FROM playlists WHERE playlist_id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return Optional.of(rs.getLong("last_updated"));
			}
			logger.info("No playlist {} found.", id);
		} catch (SQLException e) {
			logger.error("SQL Query error: {}", e.getMessage());
		}
		return Optional.empty();
	}

	/**
	 * Stores a list of songs as a playlist. Will update the playlist if it already exists.
	 * @param playlistId The unique ID
	 * @param songs The list of songs to store (can be empty, but not null
	 * @throws SQLException Any exception storing data (can probably be handled gracefully)
	 */
	public static void storePlaylist(@NonNull String playlistId, @NonNull String[] songs) throws SQLException {
		try {
			String sql = "REPLACE INTO playlists(playlist_id, last_updated, songs) VALUES(?, ?, ?)";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, playlistId);
			pstmt.setLong(2, Instant.now().getEpochSecond());
			pstmt.setString(3, String.join(",", songs));
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Update error: {}", e.getMessage());
			throw e;
		}
	}

	/**
	 * Updates a guild list with song played times. Overwrites if already exists
	 * @param guildId The guild ID
	 * @param playlistId The playlist ID
	 * @param songs A map of songs to their last played time - null entries will be skipped (but probably shouldn't exist)
	 * @throws SQLException Any exception storing data (can probably be handled gracefully)
	 */
	public static void updateGuildList(@NonNull String guildId, @NonNull String playlistId, @NonNull Map<String, Long> songs) throws SQLException {
		try {
			String sql;
			if (getGuildList(guildId, playlistId).isEmpty())
				sql = "INSERT INTO g_lists(played_list, guild_id, playlist_id) VALUES(?, ?, ?)";
			else
				sql = "UPDATE g_lists SET played_list = ? \n" +
						"WHERE guild_id = ? AND playlist_id = ?;";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, stringFromMap(songs));
			pstmt.setString(2, guildId);
			pstmt.setString(3, playlistId);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Update error: {}", e.getMessage());
			throw e;
		}
	}

	/**
	 * Retrieve a map of song played times for a guild playlist
	 * Note: will return empty on SQL errors - handle this
	 * @param guildId The guild ID
	 * @param playlistId The playlist ID
	 * @return A map, with any items found (could be empty)
	 */
	@NonNull
	public static Map<String, Long> getGuildList(@NonNull String guildId, @NonNull String playlistId) {
		String sql = "SELECT * "
				+ "FROM g_lists WHERE guild_id = ? AND playlist_id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guildId);
			pstmt.setString(2, playlistId);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return mapFromString(rs.getString("played_list"));
			}
		} catch (SQLException e) {
			logger.error("SQL Query error: {}", e.getMessage());
		}
		return new HashMap<>();
	}

	/**
	 * Adds a batch of events to the DB
	 * @param guildId The guild ID for the events
	 * @param gameId The game ID for the events
	 * @param events A list of events
 	 * @throws SQLException Any error in adding the rows - probably can be handled gracefully
	 */
	public static void addEvents(@NonNull String guildId, @NonNull String gameId, @NonNull List<Event> events) throws SQLException {
		String insertSQL = "INSERT INTO events(guild_id, game_id, event_time, code, primary_data, secondary_data) VALUES(?, ?, ?, ?, ?, ?);";
		if (events.isEmpty()) {
			return;
		}
		try {
			PreparedStatement pstmt = conn.prepareStatement(insertSQL);
			for (Event e : events) {
				pstmt.setString(1, guildId);
				pstmt.setString(2, gameId);
				pstmt.setLong(3, e.getEventTime());
				pstmt.setString(4, e.getCode());
				pstmt.setString(5, e.getPrimaryData());
				pstmt.setString(6, e.getSecondaryData());
				pstmt.addBatch();
			}
			pstmt.executeBatch();
		} catch (SQLException e) {
			logger.error("SQL Insert Error: {}", e.getMessage());
			throw e;
		}
	}

	/**
	 * Returns a song object from the database, if it exists
	 * Will return empty on a DB error
	 * @param id The spotify ID of the song
	 * @return The {@link Song} object
	 */
	@NonNull
	public static Optional<Song> getSong(@NonNull String id) {
		String sql = "SELECT * "
				+ "FROM songs WHERE song_id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				//TODO: Add way to disable songs
				Base64.Decoder decoder = Base64.getDecoder();
				String[] artists = Arrays.stream(rs.getString("artists").split(","))
						.map(s -> new String(decoder.decode(s))).toArray(String[]::new);
				return Optional.of(new Song(id, rs.getString("title"), artists, rs.getString("img_url"), rs.getString("preview_url")));
			}
		} catch (SQLException e) {
			logger.error("SQL Query error: {}", e.getMessage());
		}
		return Optional.empty();
	}

	/**
	 * Add a batch of songs into the database, encoding the artists as base64
	 * @param songs A list of song objects
	 * @throws SQLException Any error with adding the songs - may be able to be handled gracefully
	 */
	public static void addSongs(@NonNull List<Song> songs) throws SQLException {
		String insertSQL = "REPLACE INTO songs(song_id, preview_url, img_url, title, artists, add_time) VALUES(?, ?, ?, ?, ?, ?);";
		if (songs.isEmpty()) {
			return;
		}
		try {
			PreparedStatement pstmt = conn.prepareStatement(insertSQL);
			for (Song s : songs) {
				Base64.Encoder encoder = Base64.getEncoder();
				String artists = Arrays.stream(s.getArtists())
						.map(a -> encoder.encodeToString(a.getBytes())).collect(Collectors.joining(","));
				pstmt.setString(1, s.getId());
				pstmt.setString(2, s.getPreviewURL());
				pstmt.setString(3, s.getImageURL());
				pstmt.setString(4, s.getTitle());
				pstmt.setString(5, artists);
				pstmt.setLong(6, Instant.now().getEpochSecond());
				pstmt.addBatch();
			}
			pstmt.executeBatch();
		} catch (SQLException e) {
			logger.error("SQL Insert Error: {}", e.getMessage());
			throw e;
		}
	}

	private static String stringFromMap(Map<String, Long> map) {
		return map.keySet().stream()
				.filter(s -> map.get(s) != null)
				.map(key -> key + "=" + map.get(key))
				.collect(Collectors.joining(","));
	}

	private static Map<String, Long> mapFromString(String str) {
		return Arrays.stream(str.split(","))
				.map(entry -> entry.split("="))
				.collect(Collectors.toMap(entry -> entry[0], entry -> Long.parseLong(entry[1])));
	}

	private static void createTables() throws SQLException {
		String createSongs = "CREATE TABLE IF NOT EXISTS songs (\n"
				+ "	song_id TEXT PRIMARY KEY,\n"
				+ "	preview_url TEXT,\n"
				+ " img_url TEXT,\n"
				+ "	title TEXT NOT NULL,\n"
				+ " artists TEXT NOT NULL,\n"
				+ " add_time INT NOT NULL\n"
				+ ");";
		String createEvents = "CREATE TABLE IF NOT EXISTS events (\n"
				+ "	id INTEGER PRIMARY KEY,\n"
				+ "	guild_id TEXT NOT NULL,\n"
				+ " game_id TEXT NOT NULL,\n"
				+ " event_time INT NOT NULL,\n"
				+ " code TEXT NOT NULL,\n"
				+ " primary_data TEXT,\n"
				+ " secondary_data TEXT\n"
				+ ");";
		String createGLists = "CREATE TABLE IF NOT EXISTS g_lists (\n"
				+ "	guild_id TEXT NOT NULL,\n"
				+ "	playlist_id TEXT NOT NULL,\n"
				+ "	played_list TEXT\n"
				+ ");";
		String createPlaylists = "CREATE TABLE IF NOT EXISTS playlists (\n"
				+ "	playlist_id TEXT PRIMARY KEY,\n"
				+ " last_updated INT NOT NULL,\n"
				+ "	songs TEXT NOT NULL\n"
				+ ");";

		Statement stmt = conn.createStatement();
		// create a new table
		stmt.execute(createSongs);
		stmt.execute(createEvents);
		stmt.execute(createGLists);
		stmt.execute(createPlaylists);
	}

	public static void main (String[] args) throws SQLException {
		connect("bot.db");
	}
}
