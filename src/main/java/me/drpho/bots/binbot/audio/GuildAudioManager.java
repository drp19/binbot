package me.drpho.bots.binbot.audio;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import discord4j.common.util.Snowflake;
import discord4j.voice.AudioProvider;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static me.drpho.bots.binbot.Game.PLAYER_MANAGER;

/**
 * Copied from https://github.com/Discord4J/Discord4J.
 */
public class GuildAudioManager {
    private static final Map<Snowflake, GuildAudioManager> MANAGERS = new ConcurrentHashMap<>();

    public static GuildAudioManager of(final Snowflake id) {
        return MANAGERS.computeIfAbsent(id, ignored -> new GuildAudioManager(id));
    }

    public static void clearGuild(Snowflake guildId) {
        MANAGERS.remove(guildId);
    }

    private final AudioPlayer player;
    private final AudioTrackScheduler scheduler;
    private final LavaPlayerAudioProvider provider;

    private GuildAudioManager(Snowflake guildId) {
        player = PLAYER_MANAGER.createPlayer();
        scheduler = new AudioTrackScheduler(player, guildId);
        provider = new LavaPlayerAudioProvider(player);

        player.setVolume(25);
        player.addListener(scheduler);
    }

    public AudioProvider getProvider() {
        return provider;
    }

    public AudioTrackScheduler getScheduler() {
        return scheduler;
    }
}
