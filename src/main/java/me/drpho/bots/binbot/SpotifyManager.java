package me.drpho.bots.binbot;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.model_objects.specification.PlaylistTrack;
import com.wrapper.spotify.model_objects.specification.Track;
import com.wrapper.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
import org.apache.hc.core5.http.ParseException;
import org.jetbrains.annotations.Nullable;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.*;

/**
 * Manages communication with the Spotify API.
 */
public class SpotifyManager {

    private static final SpotifyApi spotifyApi;

    final static private Logger logger = LoggerFactory.getLogger(SpotifyManager.class);

    //Set up credentials to use the API
    static {
        Map<String, String> env = System.getenv();
        spotifyApi = new SpotifyApi.Builder()
                .setClientId(env.get("SPOTIFY_ID"))
                .setClientSecret(env.get("SPOTIFY_SECRET"))
                .build();
        try {
            ClientCredentialsRequest clientCredentialsRequest = spotifyApi.clientCredentials().build();
            ClientCredentials clientCredentials = clientCredentialsRequest.execute();
            spotifyApi.setAccessToken(clientCredentials.getAccessToken());
        } catch (IOException | SpotifyWebApiException | ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Default constructor.
     * Tries to retrieve the playlist from the database.
     * If it needs to get it from spotify, it will try to use the song objects from the database instead.
     * @param playlistID The ID of the playlist this game will use.
     */
    public static List<Song> getSongs(String playlistID, boolean forceUpdate) {
        List<Song> songs = new ArrayList<>();
        String[] songIds = readCache(playlistID, forceUpdate);
        if (songIds == null) {
            List<String> newSongIds = new ArrayList<>();
            logger.info("Pulling playlist from spotify");
            try {
                //Get all the songs from Spotify
                //We can only get 100 at a time so we keep doing so until less than 100 are received
                int tracksReceived = 100, offset = 0;
                List<Song> newlyAdded = new ArrayList<>();
                while (tracksReceived == 100) {
                    Paging<PlaylistTrack> trackBatch = spotifyApi.getPlaylistsItems(playlistID).offset(offset).build().execute();
                    for (PlaylistTrack playlistTrack : trackBatch.getItems()) {
                        Track track = (Track) playlistTrack.getTrack();
                        newSongIds.add(track.getId());
                        Optional<Song> dbSong = DataManager.getSong(track.getId());
                        Song toAdd;
                        if (!dbSong.isPresent()) {
                            String preview = findPreviewURL(track);
                            toAdd = new Song(track, preview);
                            newlyAdded.add(toAdd);
                            if (preview != null) {
                                songs.add(toAdd);
                            }
                            else {
                                logger.warn("Could not retrieve preview URL for song: {}", track.getName());
                            }
                        } else {
                            toAdd = dbSong.get();
                            if (toAdd.getPreviewURL() == null) {
                                String preview = findPreviewURL(track);
                                if (preview != null) {
                                    toAdd = new Song(track, preview);
                                    newlyAdded.add(toAdd);
                                    songs.add(toAdd);
                                } else {
                                    logger.warn("Could not retrieve preview URL for song: {}", track.getName());
                                }
                            } else {
                                songs.add(toAdd);
                            }
                        }
                    }
                    tracksReceived = trackBatch.getItems().length;
                    offset += 100;
                    logger.info("+100 songs");
                }
                try {
                    DataManager.addSongs(newlyAdded);
                    DataManager.storePlaylist(playlistID, newSongIds.toArray(new String[] {}));
                } catch (SQLException e) {
                    logger.warn("Was unable to add songs/new playlist to the database!");
                }
            } catch (IOException | SpotifyWebApiException | ParseException e) {
                e.printStackTrace();
            }
        } else {
            logger.info("Read {} songs from cache", songIds.length);
            List<Song> fixedSongs = new ArrayList<>();
            for (String s: songIds) {
                Optional<Song> song = DataManager.getSong(s);
                Song toAdd;
                if (!song.isPresent()) {
                    logger.warn("Song {} was in playlist but not saved!", s);
                    try {
                        Track track = spotifyApi.getTrack(s).build().execute();
                        String preview = findPreviewURL(track);
                        toAdd = new Song(track, preview);
                        if (preview != null) {
                            songs.add(toAdd);
                        }
                        else {
                            logger.warn("Could not retrieve preview URL for song: {}", track.getName());
                        }
                    } catch (IOException | ParseException | SpotifyWebApiException e) {
                        e.printStackTrace();
                    }
                } else {
                    toAdd = song.get();
                    if (toAdd.getPreviewURL() == null) {
                        String preview = findPreviewURL(toAdd.getFullURL(), 1);
                        if (preview != null) {
                            toAdd = new Song(toAdd.getId(), toAdd.getTitle(), toAdd.getArtists(), toAdd.getImageURL(), preview);
                            fixedSongs.add(toAdd);
                        }
                    }
                    if (toAdd.getPreviewURL() != null) {
                        songs.add(toAdd);
                    }
                }
            }
            try {
                DataManager.addSongs(fixedSongs);
            } catch (SQLException e) {
                logger.warn("Was unable to add songs to the database!");
            }

        }
        return songs;
    }

    /**
     * Attempt to find file in cache, otherwise just do nothing.
     * If cache is more than 1 month old, refresh
     * @param playlistId Playlist id
     * @return Null if it could not be read. Otherwise, an array of song IDs
     */
    @Nullable
    private static String[] readCache(String playlistId, boolean forceUpdate) {
        Optional<Long> cachedAt = DataManager.getUpdatedTime(playlistId);
        if (!cachedAt.isPresent()) {
            logger.info("Playlist not in cache.");
            return null;
        }
        if (System.currentTimeMillis()/1000 - cachedAt.get() > 1000L*60*60*24*31) {
            logger.info("Playlist too old.");
            return null;
        }
        if (forceUpdate) {
            logger.info("Force refreshing playlist cache");
            return null;
        }
        return DataManager.getPlaylist(playlistId);
    }

    private static String findPreviewURL(String url, int retries) {
        try {
            //Scrape Spotify's embed page of this song for the field "preview_url"
            String embedURL = url.replace("com/track", "com/embed/track");
            Document doc = Jsoup.connect(embedURL).get();
            Element resourceElement = doc.getElementById("resource");
            String resourceString = resourceElement.childNode(0).toString();
            try {
                resourceString = java.net.URLDecoder.decode(resourceString, StandardCharsets.UTF_8.name());
            } catch (UnsupportedEncodingException e) {
                // not going to happen - value came from JDK's own StandardCharsets
            }
            JSONObject resourceJSON = new JSONObject(resourceString);

            //Sometimes preview_url will be null for no reason, but trying one more time does the trick
            try {
                return resourceJSON.getString("preview_url");
            } catch (JSONException jsonException) {
                if (retries == 0) {
                    return null;
                }
                return findPreviewURL(url, retries-1);
            }
        } catch (IOException | NullPointerException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Gets the preview URL for a track via web scraping if it's null in the track object.
     * Basically just correcting for a weird quirk in Spotify's API.
     * (See https://github.com/spotify/web-api/issues/148)
     * @param track The track to get the preview URL of.
     * @return The track's preview URL
     */
    private static String findPreviewURL(Track track) {
        //If the track object already has a non-null preview URL, use it
        if (track.getPreviewUrl() != null) return track.getPreviewUrl();
        return findPreviewURL(track.getExternalUrls().get("spotify"), 2);
    }

}
