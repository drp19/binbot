package me.drpho.bots.binbot;

import discord4j.core.spec.EmbedCreateSpec;

public class EmbedCreator {

	public static EmbedCreateSpec initialMessage(EmbedCreateSpec spec, String vcName) {
		spec.setAuthor("Binb", null, null);
		spec.setTitle("Thanks for playing Binb!");
		spec.setDescription(String.format("Join the '%s' voice channel to play.\n", vcName) +
				"Further instructions will be DM'd to you.\n" +
				"If you do not get a DM, make sure you have \n" +
				"'allow DMs from server members' checked in settings.");
		spec.addField("Note: Playlists are cached!", "Playlists are cached and only \n" +
				"updated occasionally. If you use a large playlist, it may take a while to load the first time,\n" +
				"and after that any updates might not be reflected for a while. \n" +
				"Use -F in your message to force a playlist update (shouldn't take very long)", false);
		spec.addField("Leaderboard", "The leaderboard will appear here after the first round.", false);
		spec.setFooter("Created by Drew and Justin", null);
		return spec;
	}

	public static EmbedCreateSpec initialDM(EmbedCreateSpec spec) {
		spec.setAuthor("Binb", null, null);
		spec.setTitle("Welcome to BinB!");
		spec.setDescription("The goal of the game is to guess the artist and song that you hear.\n" +
				"You get more points for being the fastest.\n" +
				"When you hear the song, type your guesses in here (song + artist separately)\n" +
				"You don't have to be exact, but pretty close.\n" +
				"You'll get a warning a few seconds before the end of the round, and at the end\n" +
				"of the round a message with what song it was.\n" +
				"Good luck!");
		spec.addField("Stopping", "If you want to stay in the channel but still receive messages,\n" +
				"type '@Binb stop' and I'll stop. If you want to keep playing, just send anything and you'll join back in.\n" +
				"You will keep your points.", false);
		spec.addField("Scoring", "1 point for song, 1 for artist, and then \n" +
				"an additional 4 for 1st, 3 for second, and 2 for third.", false);
		spec.setFooter("Created by Drew and Justin", null);
		return spec;
	}

	public static EmbedCreateSpec updatedMessage(EmbedCreateSpec spec, String vcName, int round, int numRounds,
												 String playerNames, String totalPoints, String roundResult,
												 String songStr, String songImage) {
		spec.setAuthor("Binb", null, null);
		spec.setTitle("Thanks for playing Binb!");
		spec.setDescription(String.format("Join the '%s' voice channel to play.\n", vcName) +
				"Further instructions will be DM'd to you.\n" +
				"If you do not get a DM, make sure you have \n" +
				"'allow DMs from server members' checked in settings.");
		spec.addField("Note: Playlists are cached!", "Playlists are cached and only \n" +
				"updated occasionally. If you use a large playlist, it may take a while to load the first time,\n" +
				"and after that any updates might not be reflected for a while. \n" +
				"Use -F in your message to force a playlist update (shouldn't take very long)", false);
		spec.addField("Leaderboard", String.format("Round %d/%d", round, numRounds), false);
		spec.addField("Player", playerNames, true);
		spec.addField("Total Points", totalPoints, true);
		spec.addField("Round Result", roundResult, true);
		spec.addField("That was: ", songStr, false);
		spec.setImage(songImage);
		spec.setFooter("Created by Drew and Justin", null);
		return spec;
	}
}
