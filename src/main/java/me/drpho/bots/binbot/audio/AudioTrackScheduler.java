package me.drpho.bots.binbot.audio;

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import discord4j.common.util.Snowflake;
import me.drpho.bots.binbot.Game;
import me.drpho.bots.binbot.GameHandler;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Schedules the playing of each audio track. Mostly copied from https://github.com/Discord4J/Discord4J.
 */
public class AudioTrackScheduler extends AudioEventAdapter {
    private final List<AudioTrack> queue;
    private final AudioPlayer player;
    private final Snowflake guild;

    public AudioTrackScheduler(final AudioPlayer player, Snowflake guildId) {
        // The queue may be modified by different threads so guarantee memory safety
        // This does not, however, remove several race conditions currently present
        queue = Collections.synchronizedList(new LinkedList<>());
        this.player = player;
        this.guild = guildId;
    }

    public boolean play(final AudioTrack track) {
        return play(track, false);
    }

    public boolean play(final AudioTrack track, final boolean force) {
        final boolean playing = player.startTrack(track, !force);

        if (!playing) {
            queue.add(track);
        }

        return playing;
    }

    public boolean skip() {
        return !queue.isEmpty() && play(queue.remove(0), true);
    }

    @Override
    public void onTrackEnd(final AudioPlayer player, final AudioTrack track, final AudioTrackEndReason endReason) {
        // Advance the player if the track completed naturally (FINISHED) or if the track cannot play (LOAD_FAILED)
        if (endReason.mayStartNext) {
            //Notify the game that the song has just ended
            GameHandler.getGame(guild).songEnded();

            //Few seconds pause between rounds
            try {
                Thread.sleep(Game.MILLIS_BETWEEN_ROUNDS);
            } catch (InterruptedException e) {
                return; // most likely due to the game ending, so ignore and return
            }

            //Go to the next song
            skip();
        }
    }

    @Override
    public void onTrackStart(AudioPlayer player, AudioTrack track) {
        //Notify the game that we've started a new song
        GameHandler.getGame(guild).updateCurrentSong(track.getInfo().uri);
    }
}
