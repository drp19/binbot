package me.drpho.bots.binbot;

import discord4j.core.DiscordClient;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.VoiceStateUpdateEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;
import discord4j.core.object.entity.channel.PrivateChannel;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.presence.Activity;
import discord4j.core.object.presence.Presence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

public class BinBot {
	final static private Logger logger = LoggerFactory.getLogger(BinBot.class);

	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("Must have one parameter with the bot token.");
			return;
		}
		try {
			DataManager.connect("bot.db");
		} catch (SQLException e) {
			logger.error("Could not load database: {}", e.getMessage());
			System.exit(1);
		}
		final DiscordClient client = DiscordClient.create(args[0]);
		final GatewayDiscordClient gateway = client.login().block();
		assert gateway != null;
		gateway.on(ReadyEvent.class)
				.subscribe(event -> {
					User self = event.getSelf();
					logger.info(String.format("Logged in as %s#%s", self.getUsername(), self.getDiscriminator()));
					gateway.updatePresence(Presence.online(Activity.playing("Binb! Mention me from a Voice Channel"))).subscribe();
					System.out.println("\n\n\n\n\n\nUse the following link to invite the bot to your server:");
					System.out.println("https://discordapp.com/oauth2/authorize?client_id=" + gateway.getSelfId().asString() + "&scope=bot&permissions=36785472");
					System.out.println("If you don't allow all the perms, the bot probably won't work right.");
				});
		registerBinbEvents(gateway);
		logger.info("Setup completed.");
		gateway.onDisconnect().block();
	}

	private static void registerBinbEvents(GatewayDiscordClient gateway) {
		gateway.on(MessageCreateEvent.class)
				.filter(event -> event.getMessage().getChannel().block() instanceof TextChannel)
				.filter(event -> event.getMessage().getUserMentionIds().stream()
						.anyMatch(id -> id.equals(gateway.getSelfId())))
				.subscribe(GameHandler::registerStartMessage);
		gateway.on(MessageCreateEvent.class)
				.filter(event -> event.getMessage().getChannel().block() instanceof TextChannel)
				.filter(event -> !event.getMember().get().isBot())
				.filter(event -> event.getMessage().getUserMentionIds().stream()
						.noneMatch(id -> id.equals(gateway.getSelfId())))
				.filter(event -> ((TextChannel)event.getMessage().getChannel().block()).getName().equals("binb"))
				.flatMap(event -> event.getMessage().delete()).subscribe();
		gateway.on(MessageCreateEvent.class)
				.filter(event -> event.getMessage().getChannel().block() instanceof PrivateChannel)
				.filter(event -> !event.getMessage().getAuthor().get().isBot())
				.subscribe(GameHandler::registerPrivateMessage);
		gateway.on(VoiceStateUpdateEvent.class)
				.filter(event -> !event.getCurrent().getMember().block().isBot())
				.subscribe(GameHandler::registerVoiceEvent);
	}
}
