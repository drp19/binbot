package me.drpho.bots.binbot;

import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.channel.PrivateChannel;

/**
 * A wrapper for the data associated with a game player.
 */
public class Player {
	private final Member member;
	private final PrivateChannel dm;

	/**
	 * Whether a player is currently receiving messages from the bot
	 */
	boolean isPlaying;

	boolean guessedSong, guessedArtist;
	double guessedTime;

	/**
	 * Score for total game and in the current round
	 */
	int score, roundScore;

	public Player(Member member, PrivateChannel dm) {
		this.member = member;
		this.dm = dm;
		isPlaying = true;
		guessedArtist = false;
		guessedArtist = false;
		guessedTime = Double.POSITIVE_INFINITY;
		score = 0;
		roundScore = 0;
	}

	/**
	 * Add current round score and existing score
	 * @return
	 */
	public int currentScore() {
		return roundScore + score;
	}

	/**
	 * Format the round result as a string to print in the leaderboard
	 * @return Formatted string
	 */
	public String roundResult() {
		String res = "";
		if (guessedTime != Double.POSITIVE_INFINITY) {
			res += String.format("%.2f sec.- ", guessedTime);
		}
		res += "+" + roundScore + " ";
		switch (roundScore) {
			case 6: res += "(1st)"; break;
			case 5: res += "(2nd)"; break;
			case 4: res += "(3rd)"; break;
			case 2: res += "(S+A)"; break;
			case 1: if (guessedArtist) res += "(A)"; else res += "(S)"; break;
		}
		return res;
	}

	/**
	 * At the end of a round, reset variables to default
	 */
	public void resetRound() {
		score += roundScore;
		roundScore = 0;
		guessedTime = Double.POSITIVE_INFINITY;
		guessedSong = guessedArtist = false;
	}

	public void sendMessage(String content) {
		dm.createMessage(content).subscribe();
	}

	public PrivateChannel getDM() {
		return dm;
	}

	public Member getMember() {
		return member;
	}

	public double getTime() {
		return guessedTime;
	}

	public boolean isPlaying() {
		return isPlaying;
	}

	public String getName() {
		return member.getDisplayName();
	}

	/**
	 * Process a song/artist guess. Adds points to round score, unless they've now guessed
	 * both song and artist, in which case the order matters and is handled by the calling class.
	 * @param content The message
	 * @param currentSong The current song
	 * @return Whether the player has now guessed both song and artist correctly.
	 */
	public synchronized boolean guess(String content, Song currentSong) {
		boolean song = false, artist = false;
		// if the song guess is new this time
		if (currentSong.correctTitle(content) && !guessedSong) {
			song = true;
			guessedSong = true;
		}
		// if the artist guess is new this time
		if (currentSong.correctArtist(content) && !guessedArtist) {
			artist = true;
			guessedArtist = true;
		}

		if (song && !guessedArtist) { // if now has song but not artist
			sendMessage("Correct song! +1");
			roundScore++;
		} else if (artist && !guessedSong) { // if now has artist but not song
			sendMessage("Correct artist! +1");
			roundScore++;
		} else if (song || artist) { // if now has song and artist
			return true;
		}
		return false;
	}
}
