package me.drpho.bots.binbot;

import com.wrapper.spotify.model_objects.specification.ArtistSimplified;
import com.wrapper.spotify.model_objects.specification.Track;

import java.text.Normalizer;
import java.util.Arrays;

/**
 * The essential information of a track (title, artists, and song URL).
 */
public class Song {

    private static final String URL_PREFIX = "https://open.spotify.com/track/";

    private static final String FALLBACK_IMG = "https://www.scdn.co/i/_global/twitter_card-default.jpg";
    private final String title, imageURL, id, previewURL;
    private final String[] artists, simpleArtists;

    /**
     * Default constructor. Makes a song from a Track object.
     * @param track The track to make this Song object from.
     */
    public Song(Track track, String previewURL) {
        this(track.getId(), track.getName(),
                Arrays.stream(track.getArtists()).map(ArtistSimplified::getName).toArray(String[]::new),
                track.getAlbum().getImages().length >= 3 ? track.getAlbum().getImages()[2].getUrl() : FALLBACK_IMG, previewURL);
    }

    /**
     * Constructs a song from raw title/artists and URL.
     * Simplifies the title and artists per {@link Song#simplifyString(String)} and {@link Song#simplifyTitle(String)}.
     * @param title   The raw title.
     * @param artists The raw artist names.
     */
    public Song(String id, String title, String[] artists, String imageURL, String previewURL) {
        this.title = title;
        this.artists = artists;
        this.simpleArtists = Arrays.stream(artists).map(Song::simplifyString).toArray(String[]::new);
        this.imageURL = imageURL;
        this.id = id;
        this.previewURL = previewURL;
    }

    public String getFullURL() {
        return URL_PREFIX + id;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getPreviewURL() {
        return previewURL;
    }

    public String[] getArtists() {
        return artists;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    /**
     * Whether the given title is a correct guess.
     * @param guess The player's guess for the title.
     * @return Whether the guess is correct.
     */
    public boolean correctTitle(String guess) {
        return closeEnough(simplifyString(simplifyTitle(title)), simplifyString(guess));
    }

    /**
     * Whether the given artist is a correct guess.
     * @param guess The player's guess for an artist.
     * @return Whether the guess is correct.
     */
    public boolean correctArtist(String guess) {
        String guessSimple = simplifyString(guess);
        return Arrays.stream(simpleArtists).anyMatch(artist -> closeEnough(artist, guessSimple) || closeEnough(artist, "the" + guessSimple));
    }

    /**
     * Removes accents and any non-alphabetic characters, and converts to lowercase.
     * @param s The original string.
     * @return The simplified string.
     */
    private static String simplifyString(String s) {
        return Normalizer.normalize(s.toLowerCase(), Normalizer.Form.NFD).replaceAll("[^a-z0-9]", "");
    }

    /**
     * Removes extra bits at the end of a title, detected by the presence of " - ..." or " (..."<br>
     * E.g. "Heebiejeebies - Bonus" => "Heebiejeebies"; "Can't Hold Us (feat. Ray Dalton)" => "Can't Hold Us"
     * @param title The original title.
     * @return The simplified title.
     */
    private static String simplifyTitle(String title) {
        int index;

        index = title.indexOf(" - ");
        if (index != -1)
            title = title.substring(0, index);

        index = title.indexOf(" / ");
        if (index != -1)
            title = title.substring(0, index);

        index = title.indexOf(" (");
        if (index != -1)
            title = title.substring(0, index);

        index = title.indexOf("(");
        if (index != -1)
            title = title.substring(title.indexOf(")") + 2);

        return title;
    }

    /**
     * Whether two strings are "close enough" to each other<br>
     * Stolen from https://www.geeksforgeeks.org/check-if-two-given-strings-are-at-edit-distance-one/
     * @param s1 One string.
     * @param s2 The other string.
     * @return Whether the edit distance between the two strings is one
     */
    private static boolean closeEnough(String s1, String s2) {
        int m = s1.length(), n = s2.length(), count = 0, i = 0, j = 0;
        if (Math.abs(m - n) > 1) return false;

        while (i < m && j < n) {
            if (s1.charAt(i) != s2.charAt(j)) {
                if (count == 1) return false;

                if (m > n) i++;
                else if (m< n) j++;
                else { i++; j++; }

                count++;
            }

            else { i++; j++; }
        }

        if (i < m || j < n) count++;

        return count <= 1;
    }

    /**
     * Print string in "title by artist" list
     * @return The string
     */
    public String formattedString() {
        return title + " by " + Arrays.stream(artists).reduce((s, s2) -> s + ", " + s2).get();
    }

    @Override
    public String toString() {
        return "Song{" +
                "title='" + title + '\'' +
                ", artists=" + Arrays.toString(artists) +
                '}';
    }
}
