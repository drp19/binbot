package me.drpho.bots.binbot;

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.playback.NonAllocatingAudioFrameBuffer;
import discord4j.common.util.Snowflake;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.entity.channel.VoiceChannel;
import discord4j.voice.AudioProvider;
import discord4j.voice.VoiceConnection;
import me.drpho.bots.binbot.audio.GuildAudioManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class Game {

	/**
	 * Used to determine if messages are valid
	 */
	private enum GameState {
		STARTING, IN_ROUND, END_ROUND
	}

	final static private Logger logger = LoggerFactory.getLogger(Game.class);

	public static final AudioPlayerManager PLAYER_MANAGER;

	/** Milliseconds between each round (song). */
	public static final int MILLIS_BETWEEN_ROUNDS = 5000;

	/** Milliseconds before giving the round almost over warning */
	public static final int MILLIS_BEFORE_WARNING = 25000;

	/** Number of rounds in a game.**/
	public static final int ROUNDS = 15;

	/** The playlist to use if none provided.**/
	private static final String DEFAULT_PLAYLIST = "5NeJXqMCPAspzrADl9ppKn";

	private VoiceConnection channelManager;
	private final VoiceChannel gameChannel;
	private final Message leaderboardMessage;

	/** The song currently being played. */
	private Song currentSong;
	private Instant startTime;

	private final Map<Snowflake, Player> players;

	private GameState state = GameState.STARTING;

	private final String playlistId;

	private int round;
	private int numRounds;

	private final GameLog gameLog;

	private List<Song> songs;

	private Map<String, Long> playedTimes;

	/**
	 * Leaderboard for a round (doesn't need to be thread safe since it's reordered after the round)
	 */
	private final List<Player> order;

	/**
	 * Whether the game is empty (used to signal the game to end the next round)
	 */
	private boolean empty = false;

	static {
		PLAYER_MANAGER = new DefaultAudioPlayerManager();
		// This is an optimization strategy that Discord4J can utilize to minimize allocations
		PLAYER_MANAGER.getConfiguration().setFrameBufferFactory(NonAllocatingAudioFrameBuffer::new);
		AudioSourceManagers.registerRemoteSources(PLAYER_MANAGER);
		AudioSourceManagers.registerLocalSource(PLAYER_MANAGER);
	}

	/**
	 * Set up audio objects and add every person in the channel to the game
	 * @param channel The channel the game is being played in
	 * @param playlistId The spotify playlist id, if provided
	 */
	public Game(VoiceChannel channel, TextChannel textChannel, Optional<String> playlistId) {
		logger.info("Starting game in VC {}", channel.getId().asString());
		players = new HashMap<>();
		order = new Vector<>();
		round = 0;
		gameChannel = channel;
		gameLog = new GameLog(channel.getGuildId().asString());
		this.playlistId = playlistId.orElse(DEFAULT_PLAYLIST);

		channel.getVoiceStates().map(voiceState -> voiceState.getMember().block())
				.filter(member -> !member.isBot())
				.subscribe(this::createPlayer);
		leaderboardMessage = textChannel.createMessage(messageCreateSpec -> messageCreateSpec.setEmbed(embedCreateSpec -> {
			EmbedCreator.initialMessage(embedCreateSpec, gameChannel.getName());
		})).block();
		gameLog.gameStart(this.playlistId, leaderboardMessage.getId().asString());
	}

	/**
	 * Send the intro message to a player and add them to the game
	 * @param member
	 */
	private void createPlayer(Member member) {
		logger.info("Adding player {}", member.getId().asString());
		Player nP = new Player(member, member.getPrivateChannel().block());
		players.put(member.getId(), nP);
		gameLog.playerJoin(member.getId().asLong());
		nP.getDM().createMessage(messageCreateSpec -> messageCreateSpec.setEmbed(EmbedCreator::initialDM)).subscribe();
	}

	/**
	 * Build objects for music and process the playlist provided.
	 * Removes recently played songs - A song has a (1.0002 - .1log(x/rounds) - .0002(x/rounds)^3) chance of being removed
	 * Adjusts the number of rounds for shorter playlists
	 */
	public void setupVoice(boolean forceUpdate) {
		songs = SpotifyManager.getSongs(playlistId, forceUpdate);
		numRounds = Math.min(ROUNDS, songs.size());
		if (numRounds == 0) {
			logger.warn("NO SONGS!! Ending game for safety");
			GameHandler.endGame(gameChannel.getGuildId());
			return;
		}
		playedTimes = DataManager.getGuildList(gameChannel.getGuildId().asString(), playlistId);
		List<String> sortedSongs = playedTimes.keySet()
				.stream().sorted((o1, o2) -> (int)(playedTimes.get(o1)-playedTimes.get(o2))).collect(Collectors.toList());
		for (int i = 0; i < sortedSongs.size(); i++) {
			if (songs.size() <= numRounds)  {
				break;
			}
			double removeChance = Math.max(0, 1.0002 - .1*Math.log10(((double)i)/numRounds) - .0002*Math.pow((((double)i)/numRounds), 3));
			if (Math.random() < removeChance) {
				logger.info("Removing {} from the queue.", sortedSongs.get(i));
				int finalI = i;
				if (!songs.removeIf(song -> song.getId().equals(sortedSongs.get(finalI)))) {
					logger.warn("Song was not in list, removing from time map.");
					playedTimes.remove(sortedSongs.get(i));
				}
			}
		}

		final AudioProvider provider = GuildAudioManager.of(gameChannel.getGuildId()).getProvider();
		channelManager = gameChannel.join(spec -> spec.setProvider(provider)).block();

		//Tells scheduler to play each song after it is loaded
		AudioLoadResultHandler audioLoadResultHandler = new AudioLoadResultHandler() {
			@Override
			public void trackLoaded(AudioTrack audioTrack) {
				GuildAudioManager.of(gameChannel.getGuildId()).getScheduler().play(audioTrack);
			}
			public void playlistLoaded(AudioPlaylist audioPlaylist) {}
			public void noMatches() {}
			public void loadFailed(FriendlyException e) {}
		};

		List<String> previewURLs = songs.stream().map(Song::getPreviewURL).collect(Collectors.toList());
		Collections.shuffle(previewURLs);
		//Load all songs in the playlist
		for (String previewURL : previewURLs) {
			PLAYER_MANAGER.loadItem(previewURL, audioLoadResultHandler);
		}
		gameLog.musicStart(previewURLs.size(), players.size());
	}

	private Song getSongFromURL(String url) {
		return songs.stream().filter(s -> s.getPreviewURL().equals(url)).findFirst().get();
	}

	public boolean containsUser(Snowflake user) {
		return players.containsKey(user);
	}

	public VoiceChannel getVC() {
		return gameChannel;
	}

	/**
	 * Read in a DM from a player and process it.
	 * If it is @binb stop, remove them from the game
	 * Otherwise, see if it is a valid guess, if so, assign points and let them know.
	 * @param author ID of the player
	 * @param message The message object of their guess.
	 */
	public void registerMessage(Snowflake author, Message message) {
		if (message.getUserMentionIds().stream().anyMatch(id -> id.equals(message.getClient().getSelfId()))) {
			if (message.getContent().toLowerCase().contains("stop")) {
				logger.info("Removing {} from game {}.", author.asString(), gameChannel.getId().asString());
				players.get(author).sendMessage("You have stopped receiving messages for this game. You can continue playing by sending any message.");
				players.get(author).isPlaying = false;
				gameLog.playerLeaveMsg(author.asLong());
			}
		} else {
			Player player = players.get(author);
			if (!player.isPlaying) {
				player.isPlaying = true;
				gameLog.playerJoinMsg(author.asLong());
			}
			if (state == GameState.IN_ROUND) {
				boolean playerFinished = player.guess(message.getContent(), currentSong);
				int pos = 0, points = 0;
				if (playerFinished) { // if player has guessed both, add them to the finish list and give them a position (preliminary)
					order.add(player);
					pos = order.size();
					player.guessedTime = ((double)message.getTimestamp().toEpochMilli() - startTime.toEpochMilli())/1000;
					points = pos == 1 ? 5 : pos == 2 ? 4 : pos == 3 ? 3 : 1;
					String suffix = pos == 1 ? "st" : pos == 2 ? "nd" : pos == 3 ? "rd" : "th";
					player.sendMessage(String.format("Correct! Finished in %.2f seconds and got %d%s. +%d", player.guessedTime, pos, suffix, points));
					logger.info("Player {} finished in {} in game {}", player.getMember().getId().asString(), pos, gameChannel.getId().asString());
				}
				GameLog.GuessData logData = new GameLog.GuessData(currentSong.correctTitle(message.getContent()), currentSong.correctArtist(message.getContent()),
						player.guessedSong, player.guessedArtist, pos, player.roundScore+points);
				gameLog.guess(author.asLong(), logData, player.guessedTime, message.getContent());
			} else {
				gameLog.nonGuessMsg(author.asLong(), message.getContent());
			}
		}
	}

	/**
	 * Send a "x seconds" reminder to all players who have not finished and who are still playing.
	 */
	private void sendWarnings() {
		players.values().stream().filter(player -> !(player.guessedSong && player.guessedArtist)).filter(Player::isPlaying)
				.forEach(player -> player.sendMessage(String.format("%d seconds remaining!", (30000-MILLIS_BEFORE_WARNING)/1000)));
	}

	/**
	 * Read the list of players in the voice chat and add/remove players as necessary.
	 * If there are no players, mark the game to end.
	 */
	public void updatePlayers() {
		List<Member> voiceMembers = gameChannel.getVoiceStates().map(voiceState -> voiceState.getMember().block()).filter(member -> !member.isBot()).collectList().block();
		// people new to the VC and game
		voiceMembers.stream().filter(member -> !players.containsKey(member.getId()))
				.forEach(this::createPlayer);
		// people new to the VC but not the game
		voiceMembers.stream().filter(member -> players.containsKey(member.getId()))
				.forEach(member -> {
					players.get(member.getId()).isPlaying = true;
					gameLog.playerRejoinVC(member.getId().asLong());
				});
		// people no longer in the VC
		players.values().stream().map(Player::getMember).filter(member -> !voiceMembers.contains(member))
				.forEach(member -> {
					players.get(member.getId()).isPlaying = false;
					gameLog.playerLeaveVC(member.getId().asLong());
				});
		empty = voiceMembers.size() == 0;
		logger.info("Game {} now has {} players", gameChannel.getId().asString(), voiceMembers.size());
	}

	/**
	 * Respond to the current song ending.
	 * - Sort the list of finishers in order of time (in case the async flow was not in the right order)
	 * - Assign points for finishing to the players
	 * - Print out the leaderboard
	 * - Send round end messages to all players in the game.
	 * - Reset players.
	 * - If no players or the last round, end the game and update DB.
	 */
	public void songEnded() {
		state = GameState.END_ROUND;
		logger.info("Song ended in game {}", gameChannel.getId().asString());
		order.sort(Comparator.comparingDouble(Player::getTime));
		for (int i = 1; i <= order.size(); i++) {
			int points = i == 1 ? 5 : i == 2 ? 4 : i == 3 ? 3 : 1;
			Player player = order.get(i-1);
			player.roundScore += points;
		}
		StringBuilder playerNames = new StringBuilder();
		StringBuilder totalPoints = new StringBuilder();
		StringBuilder roundResult = new StringBuilder();
		Player topPlayer = null;
		int pos = 0;
		Map<Long, GameLog.RoundData> logData = new LinkedHashMap<>();
		for (Iterator<Player> it = players.values().stream().sorted((f1, f2) -> Integer.compare(f2.currentScore(), f1.currentScore())).iterator(); it.hasNext(); ) {
			Player player = it.next();
			pos++;
			if (topPlayer == null) topPlayer = player;
			playerNames.append(player.getName()).append("\n");
			totalPoints.append(player.currentScore()).append("\n");
			roundResult.append(player.roundResult()).append("\n");
			logData.put(player.getMember().getId().asLong(),
					new GameLog.RoundData(player.guessedSong, player.guessedArtist, order.indexOf(player)+1, player.roundScore, pos, player.currentScore()));
		}
		leaderboardMessage.edit(messageEditSpec -> messageEditSpec.setEmbed(embedCreateSpec -> {
			EmbedCreator.updatedMessage(embedCreateSpec, gameChannel.getName(), round, numRounds, playerNames.toString(),
					totalPoints.toString(), roundResult.toString(), currentSong.formattedString(), currentSong.getImageURL());
		})).subscribe();
		gameLog.songEnd(logData);

		// reset player values
		players.values().forEach(Player::resetRound);
		players.values().stream().filter(Player::isPlaying)
			.forEach(player -> player.sendMessage(String.format("Round over! That was %s", currentSong.getFullURL())));
		// game over - disconnect, send end message, delete
		if (round == numRounds || players.values().stream().noneMatch(Player::isPlaying) || empty) {
			String endReason = round == numRounds ? "" : "(everyone left)";
			int endReasonInt = round == numRounds ? 0 : empty ? 1 : 2;
			Player finalTopPlayer = topPlayer;
			leaderboardMessage.getChannel().subscribe(channel -> channel.createMessage(
				spec -> {
					spec.setContent(String.format("Game over %s- %s won the game with %d points! @Binb again to start a new game.", endReason, finalTopPlayer.getMember().getMention(), finalTopPlayer.score));
					spec.addFile(leaderboardMessage.getId().asString() + ".csv", new ByteArrayInputStream(gameLog.getEventLogAsString().getBytes(StandardCharsets.UTF_8)));
				}).subscribe());
			channelManager.disconnect().subscribe();
			gameLog.gameOver(finalTopPlayer.getMember().getId().asLong(), finalTopPlayer.score, endReasonInt);
			GameHandler.endGame(gameChannel.getGuildId());
			try {
				DataManager.updateGuildList(gameChannel.getGuildId().asString(), playlistId, playedTimes);
			} catch (SQLException e) {
				logger.warn("Exception when updating played times: {}", e.getMessage());
			}
			logger.info("Ending game {}", gameChannel.getId().asString());
		}
		gameLog.flushEventBuffer();
	}
	/**
	 * Update the current song from its preview URL.
	 * Set a timer for the warning messages to be displayed, and increment the round number.
	 * Reset round parameters.
	 * @param previewURL The preview URL of the song currently playing.
	 */
	public void updateCurrentSong(String previewURL) {
		state = GameState.IN_ROUND;
		currentSong = getSongFromURL(previewURL);
		playedTimes.put(currentSong.getId(), Instant.now().getEpochSecond());
		logger.info("Game {} has new song {}", gameChannel.getId().asString(), currentSong);
		Runnable timer = () -> {
			try {
				Thread.sleep(MILLIS_BEFORE_WARNING);
				sendWarnings();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		};
		Thread t = new Thread(timer);
		t.start();
		startTime = Instant.now();
		order.clear();
		round++;
		gameLog.songStart(round, currentSong.getId(), currentSong.formattedString());
	}

}
