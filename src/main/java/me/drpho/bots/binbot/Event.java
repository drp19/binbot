package me.drpho.bots.binbot;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Final immutable class to represent an event for ease of transport
 */
@AllArgsConstructor
@Getter
public final class Event {
	private final String code;
	private final Long eventTime;
	private final String primaryData, secondaryData;
}
