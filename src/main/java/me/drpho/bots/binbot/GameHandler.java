package me.drpho.bots.binbot;

import discord4j.common.util.Snowflake;
import discord4j.core.event.domain.VoiceStateUpdateEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.VoiceState;
import discord4j.core.object.entity.channel.TextChannel;
import discord4j.core.object.entity.channel.VoiceChannel;
import me.drpho.bots.binbot.audio.GuildAudioManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameHandler {

	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	private static final String PLAYLIST_REGEX_STR = "playlist[/:](\\w+)";
	private static final Pattern PLAYLIST_REGEX = Pattern.compile(PLAYLIST_REGEX_STR);

	private static final Map<String, Game> GAMES = new HashMap<>();

	/**
	 * Starts the game. Checks if a game is already in progress for a server, if so, does nothing.
	 * Otherwise, checks for an associated Spotify playlist.
	 * @param event A message event in which the bot was mentioned in a normal text channel.
	 */
	public static void registerStartMessage(MessageCreateEvent event) {
		assert event.getMember().isPresent();
		assert event.getGuildId().isPresent();

		String guildId = event.getGuildId().get().asString();
		if (GAMES.get(guildId) != null) {
			logger.info("Server {} already has a game running.", guildId);
			event.getMessage().delete().block();
			return;
		}
		if (!((TextChannel)event.getMessage().getChannel().block()).getName().equals("binb")) {
			logger.info("Not in binb channel.");
			return;
		}

		Matcher playlistMatcher = PLAYLIST_REGEX.matcher(event.getMessage().getContent());
		Optional<String> playlistId = Optional.empty();
		if (playlistMatcher.find()) {
			playlistId = Optional.of(playlistMatcher.group(1));
		}

		VoiceState authorVoiceState = event.getMember().get().getVoiceState().block();
		VoiceChannel authorVoiceChannel = authorVoiceState == null ? null : authorVoiceState.getChannel().block();
		if (authorVoiceChannel != null) {
			Game game = new Game(authorVoiceChannel, (TextChannel) event.getMessage().getChannel().block(), playlistId);
			GAMES.put(guildId, game);
			game.setupVoice(event.getMessage().getContent().toLowerCase().contains("-f"));

			logger.info("Game ready.");
		} else {
			logger.info("User {} is not in a voice channel.", event.getMember().get().getId().asString());
		}
	}

	/**
	 * Reads a private message from a user. Attempts to find which game the user is in, and then sends
	 * the message to that for processing.
	 * @param event A message in a private channel with the bot and not from the bot.
	 */
	public static void registerPrivateMessage(MessageCreateEvent event) {
		assert event.getMessage().getAuthor().isPresent();
		Snowflake authorId = event.getMessage().getAuthor().get().getId();
		Optional<Game> messageGame = GAMES.values().stream().filter(game -> game.containsUser(authorId)).findFirst();
		if (messageGame.isPresent()) {
			messageGame.get().registerMessage(authorId, event.getMessage());
		} else {
			logger.info("Received PM from user not currently in a game.");
		}
	}

	/**
	 * Processes a change in voice state.
	 * @param event A voice event change that is not a bot.
	 */
	public static void registerVoiceEvent(VoiceStateUpdateEvent event) {
		VoiceChannel channel = event.getCurrent().getChannel().block();
		VoiceChannel oldChannel = event.getOld().map(voiceState -> voiceState.getChannel().block()).orElse(null);
		Optional<Game> messageGame = GAMES.values().stream().filter(game -> game.getVC().equals(channel) || game.getVC().equals(oldChannel)).findFirst();
		if (messageGame.isPresent()) {
			messageGame.get().updatePlayers();
		} else {
			logger.info("Voice event not for a game channel.");
		}
	}

	/**
	 * retrieves a Game for the given guild
	 * @param guildId Snowflake Id
	 * @return A Game object (or null)
	 */
	public static Game getGame(Snowflake guildId) {
		return GAMES.get(guildId.asString());
	}

	/**
	 * Removes a game from the list - should already have removed itself from the list, at which point it should be GC'd
	 * @param guildId Snowflake id
	 */
	public static void endGame(Snowflake guildId) {
		GAMES.remove(guildId.asString());
		GuildAudioManager.clearGuild(guildId);
	}
}
